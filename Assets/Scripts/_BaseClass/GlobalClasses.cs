﻿
[System.Serializable]
public class SharedData
{
    public int levelIndex = 0;
}

public class Constant
{
    public const int EXECUTION_ORDER_GameManager = -1000;
    public const int EXECUTION_ORDER_StateMachine = -900;
    public const int EXECUTION_ORDER_StayInTheLightBehaviour = -100;
}